import os.path
import facemorpher
from typing import List

TOTAL_USER_COUNT = 994
# Sessions that are valid for morphing, manually ordered the session list
# to prioritise the selection of the face image to be morphed for each user
IMAGE_SESSIONS = ['941121', '940128', '940928', '940422', '940307', '940519', '960530',
				  '960620', '941031', '941201', '941205', '960627', '931230']

# I think that there is a underlining problem when the Facemorpher library is built
# The required packages to be installed for it to work does not include OpenCV on the list
# However, it does need OpenCV and a specific version of it, so was a bit tricky to get it working on macOS
# But, cannot make it work on Windows at all due to the OpenCV dependency,
# the library was built on macOS, maybe that's why
# Paths: using macOS
feret_dataset_root_path = "/data/datasets/FERET_dataset"
output_root_path = "/data/datasets/FERET_dataset_morphed_part8"
output_image_list_file = "/data/datasets/FERET_dataset/image_list.txt"


def gen_image_session_list(dataset_path: str = feret_dataset_root_path) -> List[str]:
	image_sessions: List[str] = []

	for root, dirs, files in os.walk(dataset_path):
		for f in files:
			if not f.startswith('.') and f.endswith("_fa.png"):
				session_name = f.split('_')[1]
				if session_name not in image_sessions:
					image_sessions.append(session_name)
					print(session_name)

	return image_sessions


def gen_image_list_from_dataset(dataset_path: str = feret_dataset_root_path, output_path: str = output_root_path):
	# Walk through the user folders
	user_count = 0
	images: List[str] = []
	log_file = open(output_image_list_file, 'w')
	for user_number in sorted(os.listdir(dataset_path)):
		user_path = os.path.join(dataset_path, user_number)
		if not user_number.startswith('.') and os.path.isdir(user_path):
			user_count += 1
			# print(user_number)
			selected_image = select_image_from_session_list(user_number, user_path)
			if selected_image is not None:
				print(selected_image)
				images.append(selected_image)
				# Write to file
				log_file.write(selected_image + '\n')

	log_file.close()
	assert (user_count == TOTAL_USER_COUNT)


def select_image_from_session_list(user_number: str, user_path: str) -> str:
	# Using the priority session list to select the best possible facial image for each user
	for session in IMAGE_SESSIONS:
		img_full_name = user_number + "_" + session + "_fa.png"
		print(img_full_name)
		for image in sorted(os.listdir(user_path)):
			if not image.startswith('.') and image.endswith("_fa.png"):
				if image == img_full_name:
					print(image)
					return image


def gen_morph_images(image_list_file: str = output_image_list_file, dataset_path: str = feret_dataset_root_path,
					 output_path: str = output_root_path):
	"""
	To process the whole dataset: morph every pair of the images
	"""
	# Generate image list
	with open(image_list_file) as f:
		image_list = [line.rstrip() for line in f]
	print(image_list)
	total_image = len(image_list)  # 906 image in total
	print("There are total " + str(len(image_list)))

	count = 0
	# Loop through all images in the list, and morph every pair
	for i, image in enumerate(image_list):
		user_name = image.split('_')[0]
		image_path = os.path.join(dataset_path, user_name, image)

		# Loop through all the rest images except the ones already done
		for other_image in image_list[i + 1:]:
			other_user_name = other_image.split('_')[0]
			other_image_path = os.path.join(dataset_path, other_user_name, other_image)
			output_image_name = 'M_' + user_name + '_' + other_user_name + '.png'
			output_image_path = os.path.join(output_path, output_image_name)
			facemorpher.averager([image_path, other_image_path], out_filename=output_image_path)


def gen_morph_images_from_user(starting_user_name: str, image_list_file: str = output_image_list_file,
							   dataset_path: str = feret_dataset_root_path, output_path: str = output_root_path):
	"""
	The process is very slow and the dataset is very big so taking a very long time
	Laptop ran out of space so had to split the process by user
	"""
	# Only run from a given user number, e.g. from user 100
	# Generate image list
	with open(image_list_file) as f:
		image_list = [line.rstrip() for line in f]
	print(image_list)
	total_image = len(image_list)  # 906 image in total
	print("There are total " + str(len(image_list)))

	count = 0
	# Loop through all images in the list, and morph every pair
	for i, image in enumerate(image_list):
		user_name = image.split('_')[0]
		user_number = int(user_name)
		if user_number < int(starting_user_name):
			print("Ignore user: " + user_name)
			continue
		image_path = os.path.join(dataset_path, user_name, image)

		# Loop through all the rest images except the ones already done
		for other_image in image_list[i + 1:]:
			other_user_name = other_image.split('_')[0]
			other_image_path = os.path.join(dataset_path, other_user_name, other_image)
			output_image_name = 'M_' + user_name + '_' + other_user_name + '.png'
			output_image_path = os.path.join(output_path, output_image_name)
			facemorpher.averager([image_path, other_image_path], out_filename=output_image_path)


# print(gen_image_session_list())
# gen_image_list_from_dataset()
# gen_morph_images()
gen_morph_images_from_user("00785")
